var {format, api, ecc, json, Fcbuffer} = Eos.modules;

var currentAccount = null;

var pirce_list = {
    c: [100, 100, 60, 40, 20, 20, 20, 20, 20, 20, 20],
    k: [100, 100, 60, 40, 20, 20, 20, 20, 20, 20, 20],
    start: [100, 20, 15, 10, 10, 10, 10, 10, 10, 10, 10],
    rose: [100, 20, 15, 10, 10, 10, 10, 10, 10, 10, 10],
    money: [100, 20, 15, 10, 10, 10, 10, 10, 10, 10, 10],
    xmas: [100, 20, 15, 10, 10, 10, 10, 10, 10, 10, 10],
    yang: [100, 20, 15, 10, 10, 10, 10, 10, 10, 10, 10]
};


// EOS RPC 网络设置
var network = {
    blockchain: 'eos',
    protocol: 'https',
    host: 'bos-api.eosauthority.com',
    port: 443,
    chainId: 'd5a3d18fbb3c084e3b1f3fa98c21014b5f3db536cc15d08f9f6479517c6a3d86'
};


var eos = scatter.eos(network, Eos);


var config = {
    scatter: "",
    currentAccount: "",
    authority: "active",
    currentOwnerPubkey: "",
    currentActivePubkey: "",
    accountVaild: null,
    regAccount: "",
    identity: "",
    eosClient: "",
    readonlyEos: "",
    amount: "0.1",
    selectedSuffix: "k",
    pubkey: "",
    account_prefix: "",
    fee: 0,
    core_liquid_balance: 0,
    ref_account : null
};


$(function () {

    for (var i in pirce_list) {
        item = pirce_list[i];
        // console.log(i,item);
        html = "<option value='" + i + "'>" + i + "</option>";
        $("#make").append(html);


        html = '<tr><td>' + i + '</td>';
        for (var j = 1; j < 5; j++) {
            html += "<td>" + item[j] + " BOS</td>";
        }
        html += "</tr>";
        $("#gridtable").append(html);
    }

    req = GetRequest();

    if (!jQuery.isEmptyObject(req)){
        config.ref_account =  req.ref;
        $("#language").attr("href","en.html"+location.search);
    }

    scatter.connect('MY_GAME_NAME').then(connected => {
        if(connected){
            login();
        }else{
            // alert(lang.connect_fail);
            myAlert(lang.connect_fail,lang.confirm);
        }
    });

})


$("#logout_button").click(function () {
    // scatter.forgetIdentity();
    scatter.forgetIdentity().then(result => {
        currentAccount = '';
        // alert(lang.logout_success);
        myAlert(lang.logout_success,lang.confirm);
        reset();
        login();
    }).catch(error => {
        // alert(lang.logout_error);
        myAlert(lang.logout_error,lang.confirm);
    });
});

$("#ownerkey_copy").click(function () {
    $("#public_key").val(config.currentOwnerPubkey);
    config.pubkey = config.currentOwnerPubkey;
    verify();
});

$("#activekey_copy").click(function () {
    $("#public_key").val(config.currentActivePubkey);
    config.pubkey = config.currentActivePubkey;
    verify();
});


$("#url_copy").click(function () {

    const range = document.createRange();
    range.selectNode(document.getElementById('ref_url'));

    const selection = window.getSelection();
    if(selection.rangeCount > 0) selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
    // alert(lang.copy_success);
    myAlert(lang.copy_success,lang.confirm);

});


function verify() {

    var ss = $("#make").children('option:selected').val();
    config.selectedSuffix = ss;

    var account_prefix = $("#account_prefix").val();
    var account_prefix_len = account_prefix.length;
    var prefix_max_len = 12 - config.selectedSuffix.length - 1;

    if (account_prefix_len == 0) {
        return false;
    }


    if (!new RegExp("^([a-z1-5.]){1,10}$").test(account_prefix)) {
        // alert(lang.account_regexp);
        myAlert(lang.account_regexp,lang.confirm);
        return false;
    }

    if (account_prefix == "bos") {
        // alert(lang.account_bos);
        myAlert(lang.account_bos,lang.confirm);
        return false;
    }


    if (account_prefix_len > prefix_max_len) {
        // alert(lang.account_prefix_1+ prefix_max_len + lang.account_prefix_2);
        myAlert(lang.account_prefix_1+ prefix_max_len + lang.account_prefix_2,lang.confirm);
        return false;
    } else {
        config.account_prefix = account_prefix;
    }

    var account_tmp = account_prefix + '.' + ss;
    if (account_tmp.length > 12) {
        // alert(lang.account_length);
        myAlert(lang.account_length,lang.confirm);
        return false;
    }

    if (account_tmp != config.regAccount) {
        config.accountVaild = null;
    }
    config.regAccount = account_tmp;
    config.fee = pirce_list[ss][account_prefix_len];
    $("#fee").html(config.fee + " BOS");

    if (config.pubkey != "" && config.pubkey.length != 53) {
        // alert(lang.publickey_error);
        myAlert(lang.publickey_error,lang.confirm);
        return false;
    }


    htmltext = "<li>"+lang.account + config.regAccount + "</li>";
    htmltext += "<li>"+lang.fee + config.fee + " BOS</li>";
    htmltext += "<li>"+lang.publickey + config.pubkey + "</li>";
    if (config.accountVaild == null) {
        htmltext += "<li class='fail'>"+lang.account_no_check+"</li>";
    } else if (config.accountVaild == true) {
        htmltext += "<li class='fail'>"+lang.account_regedited+"</li>";
    } else {
        htmltext += "<li class='success'>"+lang.account_unregedit+"</li>";
    }

    $("#order-check").html(htmltext);

    if (config.core_liquid_balance < config.fee){
        // alert(lang.balance_error);
        myAlert(lang.balance_error,lang.confirm);
        return false;
    }

    if (config.account_prefix != '' && config.selectedSuffix != '' && config.regAccount != '' && config.fee > 0 && config.core_liquid_balance > config.fee && config.pubkey != "" && config.accountVaild == false) {
        $("#confirm-btn").removeAttr('disabled');
        $("#confirm-btn").removeClass("disabled-btn");
        return true;
    } else {
        $("#confirm-btn").attr('disabled', 'disabled');
        $("#confirm-btn").addClass("disabled-btn");
        return false;
    }


}


function reset() {

    config.accountVaild = null;
    config.currentAccount = null;

    htmltext = "<li>" +lang.account+ config.regAccount + "</li>";
    htmltext += "<li>"+lang.fee + config.fee + " BOS</li>";
    htmltext += "<li>"+lang.publickey+ config.pubkey + "</li>";
    htmltext += "<li class='fail'>"+lang.account_no_check+"</li>";

    $("#order-check").html(htmltext);

    $("#balance").html("0 BOS");
    config.currentOwnerPubkey = '';
    config.currentActivePubkey = '';
    $("#ownerkey").html(config.currentOwnerPubkey);
    $("#activekey").html(config.currentActivePubkey);

    $("#confirm-btn").attr('disabled', 'disabled');
    $("#confirm-btn").addClass("disabled-btn");

    $('#currentaccount').html('');
    $('#ref_url').html(lang.ref_str+"https://bos.dgshds.com");
    $('#logout_button').hide();

}


function login() {
    // 登录，获取 EOS 账户
    scatter.getIdentity({accounts: [network]}).then(result => {
        // console.log(result);
        config.currentAccount = result.accounts[0].name;
        config.authority = result.accounts[0].authority;
        $('#currentaccount').html(config.currentAccount);
        $('#ref_url').html(lang.ref_str+"https://bos.dgshds.com/?ref="+config.currentAccount);
        $('#logout_button').show();
        // console.log(result.publicKey);

        getaccount(config.currentAccount);
        // alert('account:'+JSON.stringify(currentAccount));
        // alert('account:'+currentAccount.name);
    }).catch(error => {
            // alert('error:' + JSON.stringify(error));
            myAlert('error:' + JSON.stringify(error),lang.confirm);
    });

}


// 转账
function transfer() {
    if (config.currentAccount == null) {
        alert(lang.login);
    }

    if (verify() == false) {
        // alert(lang.verify_error1);
        myAlert(lang.verify_error1,lang.confirm);
        return false;
    }

    var eos = scatter.eos(network, Eos);

    var memo_str = config.account_prefix + '-' + config.selectedSuffix + '-' + config.pubkey

    if (config.ref_account != null){
        memo_str += '-' + config.ref_account; 
    }

    eos.transaction({
        actions: [
            {
                account: 'eosio.token',
                name: 'transfer',
                authorization: [{
                    actor: config.currentAccount,
                    permission: config.authority
                }],
                data: {
                    from: config.currentAccount,
                    to: 'xmas',
                    quantity: config.fee + '.0000 BOS',
                    memo: memo_str
                }
            }
        ]
    }).then(result => {
        // alert(lang.regedit_success);
        myAlert(lang.regedit_success,lang.confirm);
        getaccount(config.currentAccount);
    }).catch(error => {
            // console.log(error);
        // alert('error:' + JSON.stringify(error));
        myAlert('error:' + JSON.stringify(error),lang.confirm);
    });
}


// 转账
function getaccount(account) {

    // var eos = scatter.eos(network, Eos);

    // eos.getCurrencyBalance({ code: "eosio.token", account: account, symbol: "EOS" }).then(result => console.log(result));

    eos.getAccount({account_name: account}).then(result => {

        // console.log(result);
        config.core_liquid_balance = result.core_liquid_balance.split(" ")[0] * 1;

        for (var i = 0; i < result.permissions.length; i++) {

            item = result.permissions[i];

            if (item.perm_name == 'active') {
                config.currentActivePubkey = item.required_auth.keys[0].key;
            }

            if (item.perm_name == 'owner') {
                config.currentOwnerPubkey = item.required_auth.keys[0].key;
            }
        }

        $("#balance").html(result.core_liquid_balance);
        $("#ownerkey").html(config.currentOwnerPubkey);
        $("#activekey").html(config.currentActivePubkey);

    });

}


function checkAccount() {
    eos.getAccount({account_name: config.regAccount}).then(result => {
        config.accountVaild = true;
        // alert(lang.accountVaild_true);
        myAlert(lang.accountVaild_true,lang.confirm);
        verify();
    }).catch(e => {
            config.accountVaild = false;
        // alert(lang.accountVaild_false);
        myAlert(lang.accountVaild_false,lang.confirm);
        verify();
    });

}


function GetRequest() {  
 var url = location.search; //获取url中"?"符后的字串  
 var theRequest = new Object();  
 if (url.indexOf("?") != -1) {  
    var str = url.substr(1);  
    strs = str.split("&");  
    for(var i = 0; i < strs.length; i ++) {  
       theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);  
    }  
 }  
 return theRequest;  
}

Date.prototype.format = function (format) {
  var date = {
      "M+": this.getMonth() + 1,
      "d+": this.getDate(),
      "h+": this.getHours(),
      "m+": this.getMinutes(),
      "s+": this.getSeconds(),
      "q+": Math.floor((this.getMonth() + 3) / 3),
      "S+": this.getMilliseconds()
  };
  if (/(y+)/i.test(format)) {
      format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  for (var k in date) {
      if (new RegExp("(" + k + ")").test(format)) {
          format = format.replace(RegExp.$1, RegExp.$1.length == 1
                          ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
      }
  }
  return format;
}

function myAlert(showtext,btnText){
    showAlert({
        text:showtext,
        btnText:btnText,
        top:'34%',
        zindex:5,
        color:'#fff'
    });
}


function list(){
      $.post("https://api.bossweden.org/v1/history/get_actions",JSON.stringify({account_name: "o.k", pos: -1, offset: -50}),function(result){
        // console.log(result);

        html = "<table class='gridtable'><tr><th>"+lang.regedit_time+"</th><th>"+lang.account+"</th><th>"+lang.fee+"</th></tr>";
        $.each(result["actions"].reverse(), function(i, row){
            try{
                data = row.action_trace.act.data;
                if (data.from == "xmas"){
                    quantity = data.quantity.split(" ")[0] / 0.3;
                    memo = data.memo.split(" ")[0];
                    time = new Date(row['block_time'].replace(/T/g, " ").replace("-","/"));
                    time.setMinutes(time.getMinutes() + 480, time.getSeconds(), 0);
                    var date = time.format('yyyy-MM-dd hh:mm:ss');

                    html += "<tr><td>"+date+"</td><td>"+memo+"</td><td>"+quantity+" BOS</td></tr>"
                }
            }catch(err){}

        });

        html +=  "</table>";

        $("#regeditlist").html(html);

      });




}